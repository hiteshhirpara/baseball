//scroll top start
	$(document).ready(function() {
		var scrollTop = $(".scrollTop");

		$(window).scroll(function() {
			var topPos = $(this).scrollTop();
			if (topPos > 100) {
			  $(scrollTop).css("opacity", "1");
			} else {
			  $(scrollTop).css("opacity", "0");
			}
		});

		$(scrollTop).click(function() {
			$('html, body').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
		
	}); 
//scroll top ends

//aos start
	$('#customer-slider').on('slide.bs.carousel', function () {
		AOS.refreshHard();
	})
	AOS.init({
		duration: 1200,            
	});
	document.addEventListener("aos:in:super-duper", function(detail){
		a=0;
		myCounter();
	});
//aos ends	
	
//key facts hover start
$(function() {
   $('.hm-num-bx').hover( function(){
	  $(this).parent().css("z-index", "10");
   },
   function(){
	  $(this).parent().css("z-index", "9");
   });
});
//key facts hover ends
	
//home slider start
$(document).ready(function(){
		$(".customer-slider").slick({
			prevArrow: "<a href='#' class='prev nav-arrow'><span class='nav-ar-ic'></span></a>",
			nextArrow: "<a href='#' class='next nav-arrow'><span class='nav-ar-ic'></span></a>",
			dots: !1,
			infinite: !0,
			centerMode: !1,
			variableWidth: !1,
			arrows: !0,
			autoplay: !0,
			speed: 1e3,
			slidesToShow: 5,
			slidesToScroll: 1,
			responsive: [{
				breakpoint: 1198,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 1
				}
			}, {
				breakpoint: 991,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			}, {
				breakpoint: 641,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			}, {
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}]
		});
	});
	
	$('.customer-slider').on('init', function(event, slick){
		AOS.refreshHard();
	});	
//home slider start	

/*----mega menu start---*/	
$(document).ready(function(){
	$('.nav-item.dropdown').hover(function() {
		if ($(window).width() > 992) {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
		}}, 
		function() {
			if ($(window).width() > 992) {
				$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);			
			}
	});  
	
	//footer dropdown
	$('.quick-links h4').on('click',function(){
		if ($(window).width() < 768) {
			$(this).next('ul').slideToggle();
			$(this).parent().toggleClass("open");
		}
	});
});
/*----mega menu ends---*/

//read more start
$(document).ready(function() {
	if ($(window).width() < 768) {
		var showChar = 55;
		var ellipsestext = "";
		var moretext = "Read&nbsp;More...";
		var lesstext = "Read&nbsp;Less.";
		$('.more').each(function() {
			var content = $(this).html();

			if(content.length > showChar) {

				var c = content.substr(0, showChar);
				var h = content.substr(showChar-0, content.length - showChar);

				var html = c + '<span class="moreelipses">'+ellipsestext+'</span><span class="morecontent"><span>' + h + '</span><a href="" class="morelink">'+moretext+'</a></span>';

				$(this).html(html);
			}
		});
		
		$(".morelink").click(function(){
			if($(this).hasClass("less")) {
				$(this).removeClass("less");
				$(this).html(moretext);
			} else {
				$(this).addClass("less");
				$(this).html(lesstext);
			}
			$(this).parent().prev().toggle();
			$(this).prev().toggle();
			return false;
		});
	}	
});
//read more ends

//Count slider part height and put it to quick-inquiry-btn margin start.
$(document).ready(function(){
	calAutomargin();
});
$(window).resize(function() {
	calAutomarginAfterload();
});	
function calAutomargin() {
    setTimeout(function() {
        var bannerHeight = $('.slider').height();
	    var dynamicMargin = (bannerHeight/2);
        $(".quick-inquiry-btn").css({
            "margin-top": dynamicMargin,
            "opacity": "1"
        });
    }, 2000);
}
function calAutomarginAfterload() {
    setTimeout(function() {
        var bannerHeight = $('.slider').height();
	    var dynamicMargin = (bannerHeight/2);
        $(".quick-inquiry-btn").css("margin-top", dynamicMargin);
    }, 500);
}
//Count slider part height and put it to quick-inquiry-btn margin end.